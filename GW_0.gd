extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var running = true
const LEVEL_LENGTH = 3000
signal level_finished

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	get_node("CanvasLayer").get_node("ProgressBar").value = get_node("Player").position.x*100/LEVEL_LENGTH
	if (get_node("Player").position.x/LEVEL_LENGTH >= 1 && running == true):
		emit_signal("level_finished")
		running = false

func _on_Button_pressed():
	running = true
