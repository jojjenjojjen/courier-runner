extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button1_pressed():
	get_tree().change_scene("res://GW_0.tscn")
	Data.stage = 1.1


func _on_Button2_pressed():
	get_tree().change_scene("res://GW_0.tscn")
	Data.stage = 1.2


func _on_Button3_pressed():
	get_tree().change_scene("res://DW_1.tscn")
	Data.stage = 2.1
