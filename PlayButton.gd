extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	print("pressed")
	if is_visible_in_tree():
		hide()
	else:
		show()


func _on_Player_game_over():
	pressed = false
	print("game over")
	print (pressed)
	show()
	print(is_visible_in_tree())
	text = "try again"
