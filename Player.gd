extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const GRAVITY = 1000.0
const DOUBLEJUMPAMOUNT = 1
const RUNSPEED = 100
var playing = false
var velocity = Vector2(0,0)
var levelLimit = 288
var jumpNr = 0
var backflip = false
var secondAbility = "roll"
var rolling = false
signal game_over
#signal loadMoreLevel
# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	print(get_node("CollisionShape2D").())
#	if !get_node("RayCast2D").is_colliding():
#		position.y += 10*delta
	move_and_slide(velocity, Vector2(0, -1), false, 4, 2)
	
	if playing:
		#if position.x > levelLimit:
		#	levelLimit += 380
		#	print("level limit reached")
		#	emit_signal("loadMoreLevel", position.x)
			
		# Fall if in the air
		if !get_node("RayCast2D").is_colliding():
			velocity.y += delta * GRAVITY
		# If i want to add different jump heights the code is right here
		#if Input.is_action_just_released("ui_up") and velocity.y < 0:
		#	velocity.y += GRAVITY * 0.1
		print(velocity.x)

		if get_node("RayCast2D").is_colliding() :
			#print(applied_force)
			get_node("AnimatedSprite").animation = "Run"
			if !(Input.is_action_pressed("ui_down") or Input.is_action_pressed("ui_up")):
				jumpNr = 0
		else:
			if velocity.y < 0:
				get_node("AnimatedSprite").animation = "Jump"
			else:
				get_node("AnimatedSprite").animation = "Fall"
				
		if (get_node("RayCast2D").is_colliding() and velocity.y > 0) or jumpNr <= DOUBLEJUMPAMOUNT :
			# Jump
			if Input.is_action_just_pressed("ui_up"):
				velocity.y = -300
				jumpNr += 1
				#print(get_node("RayCast2D").get_collider().name)
				#if  == ""
				#velocity.y = -300
				#jumpNr += 1
				#pass
			#apply_central_impulse(Vector2(0,-400))
			
		# Do a backflip!
		if Input.is_action_just_pressed("ui_left") and backflip == false:
			backflip = true
			velocity.y -= 280
			velocity.x -= 100
		
		
		#Second ability, Dash or Roll
		if Input.is_action_just_pressed("ui_right"):
			if secondAbility == "dash":
				velocity.x += 300
				get_node("DashTimer").start()
			elif secondAbility == "roll":
				if rolling == false:
					velocity.x += 150
					#get_node("RollTimer").start()
					get_node("PlayerCollision").scale.y = 0.7
					rolling = true
				else:
					rolling = false
					position.y -= 10 # Prevents the player from falling into the ground when the collision shape changes
					get_node("AnimatedSprite").rotation = 0
					get_node("PlayerCollision").scale.y = 1.21
					velocity.x = RUNSPEED
			
		if position.y > 200 and playing == true:
			playing = false
			emit_signal("game_over")
			
		# Stops the backflip after one rotation
		if backflip == true:
			get_node("AnimatedSprite").rotate(-7*PI*delta)
			if get_node("AnimatedSprite").rotation < -2*PI:
				get_node("AnimatedSprite").rotation = 0
				velocity.x = RUNSPEED
				backflip = false
			
		# Stops rolling after 3 roations
		if rolling == true:
			get_node("AnimatedSprite").rotate(7*PI*delta)
			##if get_node("AnimatedSprite").rotation > 6*PI:
			##	get_node("AnimatedSprite").rotation = 0
			##	rolling = false
			##	velocity.x = RUNSPEED
		#Inte alls bra kod. Gör inte så här!
		#print(position.x)
		#if position.x < -0.5 and get_node("RayCast2D").is_colliding():
		#	position.x += 10 * delta
		#if position.x > 0.5 and get_node("RayCast2D").is_colliding():
		#	position.x -= 10 * delta
			

func _on_DashTimer_timeout():	# Slows the player down if it is running faster than normal (e.g. dashing)
	if velocity.x > RUNSPEED:
		velocity.x -= 50
		print("decreases by 50")
		if velocity.y > 0: # Keeps the player in the air while moving faster than normal (i.e. dashing)
			velocity.y = -15
	else:
		velocity.x = RUNSPEED
		get_node("DashTimer").stop()


# Start running
func _on_Button_pressed():
	playing = true
	get_node("AnimatedSprite").playing = true
	velocity = Vector2(RUNSPEED,0)
	get_node("Camera2D").set_limit(MARGIN_RIGHT,1000000)

