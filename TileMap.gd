extends TileMap

var moving = true
var levels
var loadSceneNr = 1
var distance = 272
var scene_instance
var number = 0
var finished = false
var finish_scene_instance

# Called when the node enters the scene tree for the first time.
func _ready():
	if Data.stage == 1.1:
		seed("world1_4".hash())
	elif Data.stage == 1.2:
		seed("world1_10".hash())
	# The following three lines are the only lines that should need to be changed when adding or removing segments
	levels = 5
	var scene = [load("res://GW_1.tscn"),load("res://GW_2.tscn"),load("res://GW_3.tscn"),load("res://GW_4.tscn"),load("res://GW_5.tscn")]
	finish_scene_instance = load("res://GW_finish.tscn").instance()
	
	scene_instance = [scene[0].instance(),scene[1].instance(),scene[2].instance(),scene[3].instance(),scene[4].instance()]
	
	for i in levels:
		scene_instance[i].set_name("scene" + str(i))
	
	if get_parent().running:
		moving = true

func _on_Area2D_body_entered(body):
	# Player passed the line in the segment, load next segment
	if (body.name == "Player"):
		if (finished):
			call_deferred("add_child",finish_scene_instance)
			finish_scene_instance.position.x = distance
		else:
			# Make sure the same segment is not showed directly again. This would remove the ground beneath the player.
			var prevNumber = number
			number = randi() % levels
			while prevNumber == number:
				number = randi() % levels
			
			call_deferred("add_child",scene_instance[number])

			# Connect the signal from the line in the next segment so loading more segments continue to work.
			scene_instance[number].get_node("Area2D").connect("body_entered", self, "_on_Area2D_body_entered")

			# Move the segment to the right place and update distance so upcoming segments do the same.
			scene_instance[number].position.x = distance
			distance += scene_instance[number].length


func _on_Node2D_level_finished():
	finished = true
